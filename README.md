Getting started with jmeter 
=====================================================================================================================================
download and installations: download and install   latest jmeter https://jmeter.apache.org/download_jmeter.cgi
                            Download and install java,and add java path to environment variables 
                            Run jmeter in GUI mode. 

Getting started with Jmeter script 
======================================================================================================================================
Download jmeter script (.jmx file) and open it in jmeter gui mode
Make changes in script by editing user defined variables and create csv file for respective parametrizing variables.

Parametrization and csv file
======================================================================================================================================
when user wanted to create multiple data or wanted to test application with multiple credentials then he needs to provide required
data externally which is nothing but Parametrization, this can be achieved by CSV-config element (feature of jmeter) open csv-config element
inside jmeter provide CSV file path and variables used to fetch the data.

